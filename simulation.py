from math import sqrt, sin, cos, pi
from re import L
from types import SimpleNamespace
from typing import List
from tqdm import tqdm
import numpy as np
from skopt.space import Real
from skopt import gbrt_minimize
from skopt.utils import use_named_args
from collections import namedtuple
from time import time

import json


class SpeedCurveGenerator:

    @classmethod
    def constant(cls, maxspeed, timedelta):
        while True:
            yield maxspeed * timedelta

    @classmethod
    def accelerate(cls, acc, maxspeed, timedelta):
        current_speed = 0
        while current_speed < maxspeed:
            current_speed += acc * timedelta
            yield current_speed * timedelta
        yield from cls.constant(maxspeed, timedelta)
    
    @classmethod
    def wrap(cls, acc, maxspeed, timedelta):
        if acc is None:
            return cls.constant(maxspeed, timedelta)
        else:
            return cls.accelerate(acc, maxspeed, timedelta)


class SimulationEntity:
    def __init__(self, startloc, speedcurve, timedelta):
        self.location = startloc
        self.history = []
        self.timedelta = timedelta
        self.xspeed = -6*timedelta
        self.yspeed = 0

        self.has_collided = False
        self.collision_ts = float("inf")
        
        self.speedcurve = SpeedCurveGenerator.wrap(*speedcurve, timedelta)
        self.current_speed = 0
        self.internal_clock = 0
        self.clock_signal = 0
        
        self.update_location(*self.location)

    def update_location(self, new_x, new_y):
        prev_x, prev_y = self.location
        self.location = new_x, new_y
        self.internal_clock += 1
        real_speed = sqrt((prev_x - new_x) ** 2 + (prev_y - new_y) ** 2)
        self.history.append(
            SimpleNamespace(
                location=self.location,
                speed=float(real_speed / self.timedelta),
                goes_left=(prev_x > new_x),
            )
        )

    def move_in_direction(self, delta_x, delta_y):
        x, y = self.location
        self.current_speed = next(self.speedcurve)
        factor = self.current_speed / sqrt(delta_x * delta_x + delta_y * delta_y)
        movement_x = delta_x * factor
        movement_y = delta_y * factor
        self.clock_signal += 1
        self.update_location(x + movement_x, y + movement_y)

    def move_towards(self, target_x, target_y):
        x, y = self.location
        delta_x = target_x - x
        delta_y = target_y - y
        self.move_in_direction(delta_x, delta_y)
        
    def orbit(self, target_x, target_y):
        x, y = self.location
        delta_x = target_x - x
        delta_y = target_y - y
        factor = 4*self.timedelta**2 / sqrt(delta_x * delta_x + delta_y * delta_y)
        acc_x = delta_x * factor
        acc_y = delta_y * factor
        self.xspeed += acc_x
        self.yspeed += acc_y
        self.update_location(x + self.xspeed, y + self.yspeed)

    @property
    def steps_behind(self):
        return self.clock_signal - self.internal_clock

    def step(self):
        raise NotImplementedError

    def collide(self, other):
        return (self.location[0] - other.location[0]) ** 2 + (
            self.location[1] - other.location[1]
        ) ** 2 < self.current_speed**2

    def sync_to(self, timestamp):
        for _ in range(self.internal_clock, timestamp):
            self.update_location(*self.location)
        assert self.internal_clock == timestamp
        
    def travel_distance(self):
        return sum(x.speed for x in self.history)
    
    def flight_distance(self):
        start_x, start_y = self.history[0].location
        end_x, end_y = self.history[-1].location
        return sqrt((start_x - end_x)**2 + (start_y - end_y)**2)

    def max_distance(self, max_time=None):
        if max_time:
            return sqrt(max((x.location[0]**2+x.location[1]**2) for x in self.history[:max_time]))
        else:
            return sqrt(max((x.location[0]**2+x.location[1]**2) for x in self.history))

class PrecogSimulationEntity(SimulationEntity):
    def __init__(self, startloc, speedcurve, timedelta):
        super().__init__(startloc, speedcurve, timedelta)
        self.undecided_travel_distance = []

    def store_movement(self):
        self.clock_signal += 1
        self.current_speed = next(self.speedcurve)
        self.undecided_travel_distance.append(self.current_speed)

    def recalculate_path(self, target_x, target_y):
        x, y = self.location
        delta_x = target_x - x
        delta_y = target_y - y
        if sqrt(delta_x * delta_x + delta_y * delta_y) < sum(
            self.undecided_travel_distance
        ):
            self.unleash(target_x, target_y)

    def unleash(self, target_x, target_y):
        x, y = self.location
        delta_x = target_x - x
        delta_y = target_y - y
        if not self.undecided_travel_distance:
            return
        if delta_x or delta_y:
            factor = 1 / sqrt(delta_x * delta_x + delta_y * delta_y)
        else:
            factor = 1
        total = 0
        for velocity in self.undecided_travel_distance:
            total += velocity
            movement_x = delta_x * factor * total
            movement_y = delta_y * factor * total
            if total * factor < 1:
                self.update_location(x + movement_x, y + movement_y)
            else:
                self.update_location(target_x, target_y)
        self.undecided_travel_distance[:] = []

    def travel_precog(self, target_x, target_y):
        self.store_movement()
        self.recalculate_path(target_x, target_y)


class Prey(SimulationEntity):
    def __init__(self, startloc, angle, speedcurve, timedelta):
        super().__init__(startloc, speedcurve, timedelta)
        self.direction = sin(angle), -cos(angle)
        self.angle = angle

    def step(self):
        self.move_in_direction(*self.direction)


def piepolar_to_cartasian(radius, part):
    return (sin(part * 2 * pi) * radius, -cos(part * 2 * pi) * radius)


class Hunter(SimulationEntity):
    def __init__(self, startloc, target, speedcurve, timedelta):
        super().__init__(startloc, speedcurve, timedelta)
        self.target = target

    def step(self):
        self.move_towards(*self.target.location)

    def sync_to_target(self):
        return self.sync_to(self.target.internal_clock)

class OrbitHunter(SimulationEntity):
    def __init__(self, startloc, target, speedcurve, timedelta):
        super().__init__(startloc, speedcurve, timedelta)
        self.target = target

    def step(self):
        self.orbit(*self.target.location)

    def sync_to_target(self):
        return self.sync_to(self.target.internal_clock)

class PrecogHunter(PrecogSimulationEntity, Hunter):
    def __init__(self, startloc, target, speedcurve, timedelta):
        Hunter.__init__(self, startloc, target, speedcurve, timedelta)
        self.undecided_travel_distance = []
    def step(self):
        self.travel_precog(*self.target.location)

    def sync_to(self, ts):
        self.unleash(*self.target.location)
        super().sync_to(ts)


class World:
    def __init__(self, human: Prey, *raptors: List[Hunter], timedelta=1/200):
        self.human = human
        self.raptors = raptors
        for raptor in self.raptors:
            raptor.target = self.human

    def run(self, max_seconds=1_000_000):
        try:
            for ts in range(int(max_seconds/self.human.timedelta)):
                self.human.step()
                for raptor in self.raptors:
                    if not raptor.has_collided:
                        raptor.step()
                        if raptor.collide(self.human):
                            raptor.has_collided = True
                            assert ts > 0, "collision at timestamp {ts}"
                            raptor.collision_ts = ts - 1
                if all(raptor.has_collided for raptor in self.raptors):
                    break
            else:
                assert True, "simulation ended prematurely"
        finally:
            for raptor in self.raptors:
                raptor.sync_to_target()
                
    def melt(self, catch_index=None):
        if catch_index is None:
            mx = len(self.human.history)-1
        else:
            mx = self.catch_times()[catch_index]
        for timestamp in range(mx): 
            yield SimpleNamespace(
                human=self.human.history[timestamp],
                raptors=[raptor.history[timestamp] for raptor in self.raptors],
                timestamp=timestamp*self.human.timedelta
            )
            
    def fitness(self):
        return min(raptor.collision_ts for raptor in self.raptors)
    
    def survive_time(self):
        return self.fitness*SimulationEntity.timedelta
    
    def entities(self):
        yield self.human
        yield from self.raptors
    
    def reach(self, mx_time=None):
        return max(x.max_distance(mx_time) for x in self.entities())
    
    def reach_before_catch(self, index):
        return self.reach(self.catch_times()[index])
    
    def raptor_len(self):
        return len(self.raptors)
    
    def catch_locations(self):
        return [self.human.history[x].location for x in self.catch_times()]
    
    def catch_times(self):
        return tuple(sorted(raptor.collision_ts for raptor in self.raptors))
            
            
def generate_surrounded_prey(radius, prey_curve, *raptor_curves):
    prey_curve = prey_curve
    raptor_curves = list(raptor_curves)
    def gen(angle, timedelta):
        n =len(raptor_curves)
        
        prey_type, *prey_curve_ = prey_curve
        yield prey_type((0,0), angle, prey_curve_, timedelta=timedelta)
        
        for i, (raptor_type, *raptor_curve) in enumerate(raptor_curves):
            yield(raptor_type(piepolar_to_cartasian(radius, i/n), None, raptor_curve, timedelta=timedelta))
    return gen
    
    
def pairs(iterator):
    iterator = iter(iterator)
    prev = next(iterator)
    
    for x in iterator:
        yield prev, x
        prev = x
        
        
def format_angle(angle):
    degs = angle/pi * 180
    degs_int = int(degs)
    mins = (degs - degs_int) * 60
    mins_int = int(mins)
    secs = (mins - mins_int) * 60
    secs_int = int(secs)
    
    return f"{degs_int}°{mins_int}'{secs_int}\""

CompressedWorld = namedtuple("CompressedWorld", "angle, tps, catch_times, catch_locations, fitness, reach, reach_before_catch")
    
class MinimumTimeExceededError(AssertionError):
    pass
    
class Universe:
    dim = [Real(name='angle', low=0.0, high=2*pi)]
    def __init__(self, layout, tps=200):
        self.tps = tps
        self.layout = layout
        self.worlds = {}
        self._best_angle = None
        
    def register_world(self, angle: float):
        if angle in self.worlds:
            return self.worlds[angle].fitness
        
        world = World(*self.layout(angle, 1/self.tps))
        
        tic = time()
        world.run()
        toc = time()
        if toc - tic > 1:
            raise MinimumTimeExceededError()
        
        compressedworld = CompressedWorld(
            angle,
            self.tps,
            tuple(world.catch_times()),
            tuple(world.catch_locations()),
            world.fitness(),
            world.reach(),
            world.reach_before_catch(0)
        )
        
        del world
        
        self.worlds[angle] = compressedworld

        return compressedworld.fitness
    
    def regenerate_best(self):
        angle = self.best_angle()
        world = World(*self.layout(angle, 1/self.tps))
        world.run()
        return world
    
    def run_world(self, tqdmlink):
        @use_named_args(self.dim)
        def func_to_optimise(angle:float) -> float:
            tqdmlink.update(1)
            return -self.register_world(angle)
        return func_to_optimise
    
    def find_best_angle(self, trials=1000, overwrite_dim=(0, 2*pi)):
        self.dim = [Real(name='angle', low=overwrite_dim[0], high=overwrite_dim[1])]
        with tqdm(total=trials, desc="optimising") as tqdmlink:
            gbrt_minimize(self.run_world(tqdmlink), self.dim, n_calls=trials, verbose=False)
        self._best_angle = None
        
        
    def scan_worlds(self, trials=60):
        for angle in tqdm(np.linspace(0, 2*pi, trials), desc="scanning"):
            self.register_world(angle)
        self._best_angle = None
            
    def explore(self, grid, trials, grid2=0, increase_tps=None):
        if grid:
            self.scan_worlds(grid)
        if increase_tps:
            self.tps = increase_tps
        if trials:
            self.find_best_angle(trials)
        if grid2:
            self.lower_granularity(2*pi/(grid2))
        acc, s = self.estimate_accuracy()
        print("interval", format_angle(acc[0]), format_angle(acc[1]), s)
            
    def lower_granularity(self, granularity: float):
        self.register_world(0)
        self.register_world(2*pi)
        self._best_angle = None
        
        with tqdm(total=int(2*pi/granularity), desc="granularity") as pbar:  
            for x,y in pairs(sorted(self.worlds.keys())):
                pbar.update(int(x/granularity) - pbar.n)  # here we update the bar of increase of cur_perc
                
                dist = (y-x)
                mult = int(dist/granularity + 1)
                new_dist = dist/mult
                if mult > 1:
                    for i in range(mult-1):
                        pbar.update(1)
                        self.register_world(x + new_dist*i)
            
            
    def best_angle(self):
        if self._best_angle is None:
            self._best_angle = max(self.worlds, key=lambda x: self.worlds[x].fitness)       
        return self._best_angle

    def reach_polygon(self, scale=0):
        return [self.worlds[a].catch_locations[scale] for a in sorted(self.worlds.keys())]
    
    def reach(self):
        return max(x.reach for x in self.worlds.values())
    
    def reach_before_catch(self):
        return max(x.reach_before_catch for x in self.worlds.values())
    
    def estimate_accuracy(self, frame_uncertainty=4):
        mark = False
        arc_start = 0
        prev_angle = 0
        marker_history = []
        optimal_timestamp = max(w.fitness for w in self.worlds.values())
        assert optimal_timestamp > 0, "timestamp cannot be negative or zero"
        lower_bound_of_optimal_timestamp = optimal_timestamp - frame_uncertainty
        for cycle in (0,):
            for angle in sorted(self.worlds.keys()):
                fitness = self.worlds[angle].fitness
                if not mark:
                    if fitness >= lower_bound_of_optimal_timestamp:
                        mark = True
                        arc_start = prev_angle + cycle
                        marker_history.append(fitness)
                else:
                    marker_history.append(fitness)
                    if fitness < lower_bound_of_optimal_timestamp:
                        mark = False
                        arc_end = angle + cycle
                        if optimal_timestamp in marker_history:
                            number_of_peak_candidates = len(marker_history)
                            uncertainty_arc = (arc_start, arc_end)
                            return  uncertainty_arc, number_of_peak_candidates       
                prev_angle = angle
        
        

def generate_tps_layers():
    current_order = 100
    while True:
        yield current_order*2
        yield current_order*5
        yield current_order*10     
        current_order *= 10           
        
        
def hyper_optimise(layout, logfile, target_precision):
    order = generate_tps_layers()
    current_tps = next(order)
    
    current_arc = (0, pi)
    target_hits = 20
    
    with open(f"log_{logfile}.csv", "w") as file:
        
        file.write("angle,tps,hit1,hit2,hit3\n")
    
        while current_arc[1] - current_arc[0] > 2*pi/target_precision:
            universe = Universe(layout, current_tps)
            
            number_of_peak_candidates = 0

            while number_of_peak_candidates < 15:
                hits_to_still_make = (target_hits - number_of_peak_candidates)
                
                universe.find_best_angle(hits_to_still_make*2, overwrite_dim = current_arc)
                try:
                    next_current_arc, number_of_peak_candidates = universe.estimate_accuracy()
                except TypeError:
                    pass
                else:
                    if current_arc[1] - current_arc[0] > next_current_arc[1] - next_current_arc[0]:
                        current_arc = next_current_arc
                print("current tps:", current_tps)
                print("current uncertainty interval:", format_angle((current_arc[0] + current_arc[1])/2), "\u00b1", format_angle(abs(current_arc[0] - current_arc[1])/2))
                print("number of peak candidates:", number_of_peak_candidates)
                best = universe.best_angle()
                print("best angle:", format_angle(best))
                print("peak:", universe.worlds[best].fitness, universe.worlds[best].fitness / current_tps)
                
            for x in sorted(universe.worlds):
                angle = x
                tps = current_tps
                hits = ",".join(map(str, universe.worlds[x].catch_times))
                
                file.write(f"{angle},{tps},{hits}\n")
                
            current_tps = next(order)
            with open(f"sol_{logfile}.json", "w") as solfile:
                json.dump(dict(
                    angle=universe.best_angle(),
                    lower_bound=current_arc[0],
                    upper_bound=current_arc[1],
                    max_tps=universe.tps,
                    hits=universe.worlds[universe.best_angle()].catch_times
                ), solfile)
        
    return universe
        
def load_world_from_solution(layout, logfile):
    with open(f"sol_{logfile}.json") as solfile:
        data = json.load(solfile)
    
    world = World(*layout(data["angle"], 1/data["max_tps"]))
    
    world.run()
    assert world.catch_times() == tuple(data["hits"]), f"{world.catch_times()} {tuple(data['hits'])} results from cache do not match"
    
    return world

    