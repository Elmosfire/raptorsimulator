import animate
import simulation
from math import pi, sqrt
from tqdm import tqdm

def draw_frame(animation: animate.Animation, tick: simulation.SimpleNamespace, repeat=1, dead=False):
    animation.register_object(
        "human_dead" if dead else "human", tick.human.location, tick.human.goes_left, f"{tick.human.speed:.2f} m/s"
    )
    for raptor in tick.raptors:
        animation.register_object(
            "raptor", raptor.location, not raptor.goes_left, f"{raptor.speed:.2f} m/s"
        )
    
    
    animation.flip_frame(repeat)
    
def draw_frame_2(animation: animate.Animation, tick: simulation.SimpleNamespace, repeat=1, dead=False):
    animation.register_object(
        "human_dead" if dead else "human", tick.human.location, tick.human.goes_left, ""
    )
    for raptor in tick.raptors:
        animation.register_object(
            "raptor", raptor.location, not raptor.goes_left, ""
        )
    
    
    animation.flip_frame(repeat)
        

def animate_universe_optimal(universe_draw: simulation.Universe, world: simulation.World, filename, title, speedup=5):
    best = world.human.angle
    with animate.Animation(filename, universe_draw.reach_before_catch()*1.1) as a:
        a.add_title(title)
        a.add_label_permanent(simulation.format_angle(best))
        
        a.draw_polygon(universe_draw.reach_polygon(2), (191, 191, 191, 255))
        a.draw_polygon(universe_draw.reach_polygon(1), (127, 127, 255, 255))
        a.draw_polygon(universe_draw.reach_polygon(), (127, 255, 127, 255))
        a.create_scale()
        ticks = list(world.melt(0))
        
        a.add_timestamp(ticks[0].timestamp)
        draw_frame(a, ticks[0], a.fps)
        
        frame_counter = 0
        for tick in tqdm(ticks[1:-1], desc="animating"):
            video_ts = frame_counter/a.fps / speedup
            sim_ts = tick.timestamp
            if sim_ts > video_ts:
                frame_counter += 1
                a.add_timestamp(tick.timestamp)
                draw_frame(a, tick)
            
        a.add_timestamp(ticks[-1].timestamp)
        draw_frame(a, ticks[-1] , a.fps*2, dead=True)
        
def animate_world(world: simulation.World, speedup=0.1):
    world.run(max_seconds=10)
    with animate.Animation("world", world.reach()*1.1) as a:
        a.add_title("orbit")
        
        a.create_scale()
        ticks = list(world.melt(None))
        
        a.add_timestamp(ticks[0].timestamp)
        draw_frame_2(a, ticks[0], a.fps)
        
        frame_counter = 0
        for tick in tqdm(ticks, desc="animating"):
            video_ts = frame_counter/a.fps / speedup
            sim_ts = tick.timestamp
            if sim_ts > video_ts:
                frame_counter += 1
                a.add_timestamp(tick.timestamp)
                draw_frame_2(a, tick)
            
            
def run_and_animate(layout, title, overwrite=None):
    
    try:
        world = simulation.load_world_from_solution(layout, title)
    except FileNotFoundError:
        universe_data = simulation.hyper_optimise(layout, title, 360*60*3)
        world = universe_data.regenerate_best()
    
    universe_draw = simulation.Universe(layout, tps=200)
    universe_draw.explore(360*8, 0, 0)
    

    animate_universe_optimal(universe_draw, world, title, title.replace(" ", "_") if not overwrite else overwrite)

            
            
    
    

wounded_raptor = (simulation.Hunter, 4, 10)
wounded_raptor2 = (simulation.Hunter, 4, 8)
wounded_raptor_ts = (simulation.Hunter, None, 10)
raptor = (simulation.Hunter, 4, 25)
raptor_sim = (simulation.OrbitHunter, None, 25)
raptor_ts = (simulation.Hunter, None, 25)
wounded_raptor_precog = (simulation.PrecogHunter, 4, 10)
raptor_precog = (simulation.PrecogHunter, 4, 25)
human = (simulation.Prey, None, 6)
human_standstil = (simulation.Prey, None, 0)

layout_default = simulation.generate_surrounded_prey(20*sqrt(3)/3, human_standstil, raptor_sim, raptor_sim, raptor_sim)
animate_world(simulation.World(*layout_default(pi/2, 1/200)), 1.2)

#layout_default = simulation.generate_surrounded_prey(20*sqrt(3)/3, human, wounded_raptor, raptor, raptor)
#run_and_animate(layout_default, "pursuit")

#layout_default = simulation.generate_surrounded_prey(20*sqrt(3)/3, human, raptor, raptor, raptor)
#run_and_animate(layout_default, "pursuit_sym", "no wounded raptor")

#layout_default = simulation.generate_surrounded_prey(20*sqrt(3)/3, human, wounded_raptor2, raptor, raptor)
#run_and_animate(layout_default, "pursuit_slow")

#layout_precog = simulation.generate_surrounded_prey(20*sqrt(3)/3, human, wounded_raptor_precog, raptor_precog, raptor_precog)
#run_and_animate(layout_precog, "precog")

#layout_default = simulation.generate_surrounded_prey(20*sqrt(3)/3, human, wounded_raptor_ts, raptor_ts, raptor_ts)
#run_and_animate(layout_default, "start at top speed", "top speed")

#layout_precog = simulation.generate_surrounded_prey(20*sqrt(3)/3, human, raptor, raptor_precog, raptor_precog)
#run_and_animate(layout_precog, "mixed")