from PIL import Image, ImageDraw, ImageFont
import numpy as np
import cv2
from pathlib import Path
import os
import subprocess as sp


def pixel_font(sz):
    return ImageFont.truetype("fonts/joystix.monospace-regular.ttf", sz)


def xkcd_font(sz):
    return ImageFont.truetype("fonts/Humor-Sans-1.0.ttf", sz)


SPRITES = {}


def load_sprite(name, ratio=0.45):
    image = Image.open(f"sprites/{name}.png")
    sx, sy = image.size

    image = image.resize((int(sx * ratio), int(sy * ratio)), Image.LANCZOS)
    SPRITES[name] = image


load_sprite("human")
load_sprite("human_dead")
load_sprite("raptor")

class FfmpegInterface:
    def __init__(self, width, height, fps, filename):
        dimension = '{}x{}'.format(width, height)

        command = ['ffmpeg',
                '-y',
                '-f', 'rawvideo',
                '-vcodec','rawvideo',
                '-s', dimension,
                '-pix_fmt', 'bgr24',
                '-r', str(fps),
                '-i', '-',
                '-an',
                '-vcodec', 'libx265',
                '-b:v', '5000k',
                str(filename) ]

        self.proc = sp.Popen(command, stdin=sp.PIPE, stderr=sp.PIPE)

    def write_frame(self, image):
        frame = np.array(image.convert("RGB"))[:, :, ::-1]
        self.proc.stdin.write(frame.astype(np.uint8).tobytes())

    def finalise(self):
        self.proc.stdin.close()
        self.proc.stderr.close()
        self.proc.wait()


class Animation:
    bgcolor = (243, 243, 243)
    title_font = pixel_font(104)
    subscript_font = pixel_font(80)
    label_font = pixel_font(30)
    scale_font = pixel_font(60)

    def __init__(self, filename, radius, pixelsize=(1920, 1080), bordersize=100, fps=25):
        self.size = pixelsize
        self.filename = Path(filename).with_suffix(".mp4")
        self.tmp = Path(filename).with_suffix(".avi")

        self.canvas = Image.new("RGBA", self.size, (255, 255, 255))

        self.object_layer = Image.new("RGBA", self.size, (255, 255, 255, 0))
        self.field_layer = Image.new("RGBA", self.size, (255, 255, 255, 0))
        self.label_layer_perm = Image.new("RGBA", self.size, (255, 255, 255, 0))
        self.label_layer = Image.new("RGBA", self.size, (255, 255, 255, 0))

        self.image = Image.new("RGBA", self.size, (255, 255, 255))

        self.objects = []

        self.border_shift = bordersize / 2

        w, h = self.size
        self.scale = min(w, h - bordersize) / radius / 2
        self.fps = fps

    def __enter__(self):
        self.video = FfmpegInterface(self.size[0], self.size[1], self.fps, self.filename)
        return self

    def __exit__(self, *_):
        self.video.finalise()

    def convert_position(self, x, y):
        w, h = self.size
        return (
            int(x * self.scale + w / 2),
            int(y * self.scale + h / 2 + self.border_shift),
        )

    def flip_frame(self, repeat=1):
        self.image = self.canvas.copy()
        self.image.paste(self.field_layer, mask=self.field_layer)
        self.image.alpha_composite(self.label_layer_perm, (0, 0))
        self.image.alpha_composite(self.label_layer, (0, 0))
        
        self.label_layer = Image.new("RGBA", self.size, (255, 255, 255, 0))
        self.flip_objects()
        
        self.image.alpha_composite(self.object_layer, (0, 0))
        for _ in range(repeat):
            self.video.write_frame(self.image)

    ############################################################
    # OBJECTS
    ############################################################

    def draw_object_sprite(self, sprite, position, mirror=False):
        image = SPRITES[sprite]

        if mirror:
            image = image.transpose(Image.FLIP_LEFT_RIGHT)

        size_x, size_y = image.size

        anchor_x, anchor_y = self.convert_position(*position)

        left, top = int(anchor_x - size_x / 2), int(anchor_y - size_y + 3)

        # If left or top is negative, we cut from the original image instead trying to draw it outside it
        cut_left, cut_top = 0, 0

        if left < 0:
            cut_left = -left
            left = 0

        if top < 0:
            cut_top = -top
            top = 0

        if cut_left or cut_top:
            image = image.crop((cut_left, cut_top, size_x, size_y))

        self.object_layer.alpha_composite(image, (left, top))

    def draw_object_shadow(self, position, size):
        anchor_x, anchor_y = self.convert_position(*position)

        poly = Image.new("RGBA", self.size)
        pdraw = ImageDraw.Draw(poly)
        pdraw.ellipse(
            [
                anchor_x - size,
                anchor_y - size // 2,
                anchor_x + size,
                anchor_y + size // 2,
            ],
            fill=(127, 127, 127, 127),
        )

        self.object_layer.paste(poly, mask=poly)

    def draw_object_label(self, position, text):

        img_txt = Image.new("RGBA", self.label_font.getsize(text))
        draw_txt = ImageDraw.Draw(img_txt)
        draw_txt.text((0, 0), text, font=self.label_font, fill=(0, 0, 0, 255))

        anchor_x, anchor_y = self.convert_position(*position)
        size_x, size_y = img_txt.size

        self.object_layer.paste(
            img_txt, (anchor_x - size_x // 2, anchor_y), mask=img_txt
        )

    def flip_objects(self):
        self.object_layer = Image.new("RGBA", self.size, (255, 255, 255, 0))
        for obj in sorted(self.objects, key=lambda obj: obj["y"]):
            self.draw_object_shadow(obj["position"], 5)
            self.draw_object_sprite(obj["sprite"], obj["position"], obj["mirror"])
            self.draw_object_label(obj["position"], obj["label"])
        self.objects[:] = []

    def register_object(self, sprite, position, mirror, label):
        self.objects.append(
            dict(
                sprite=sprite,
                position=position,
                y=position[1],
                mirror=mirror,
                label=label,
            )
        )

    ############################################################
    # COLORED FIELD
    ############################################################

    def mark_center(self):
        px, py = self.convert_position(0, 0)
        poly = Image.new("RGBA", self.size)
        pdraw = ImageDraw.Draw(poly)
        pdraw.ellipse([px - 5, py - 2, px + 5, py + 2], fill=(0, 255, 0, 255))
        self.field_layer.paste(poly, mask=poly)

    def draw_polygon(self, points, color=(0, 255, 0, 127)):
        
        poly = self.field_layer
        pdraw = ImageDraw.Draw(poly)
        pdraw.polygon([self.convert_position(x, y) for x, y in points], fill=color)
        self.mark_center()

    ############################################################
    # LEGEND AND LABELS
    ############################################################

    @staticmethod
    def text_as_image(text, font):
        img_txt = Image.new("RGBA", font.getsize(text))
        draw_txt = ImageDraw.Draw(img_txt)
        draw_txt.text((0, 0), text, font=font, fill=(0, 0, 0, 255))
        return img_txt

    def put_text_in_corner(
        self, text, font, right=False, bottom=False, border=10, permanent=False
    ):
        img_txt = self.text_as_image(text, font)
        x = self.size[0] - img_txt.size[0] - border if right else border
        y = self.size[1] - img_txt.size[1] - border if bottom else border
        if permanent:
            self.label_layer_perm.paste(img_txt, (x, y), mask=img_txt)
        else:
            self.label_layer.paste(img_txt, (x, y), mask=img_txt)

    def add_timestamp(self, ts, speedup=1):
        minutes, seconds = divmod(int(ts), 60)
        centisecs = int(ts * 100) % 100
        text = f"{minutes:02}:{seconds:02}.{centisecs:02}"
        if speedup > 1:
            text = f"(/{speedup:.0f}) " + text
        elif speedup < 1:
            text = f"(x{1/speedup:.0f}) " + text

        self.put_text_in_corner(text, self.subscript_font, False, True)

    def add_label(self, text):
        self.put_text_in_corner(text, self.subscript_font, True, True)
        
    def add_label_permanent(self, text):
        self.put_text_in_corner(text, self.subscript_font, True, True, permanent=True)

    def add_title(self, text):
        self.put_text_in_corner(text, self.title_font, False, False, permanent=True)

    def create_scale(self):
        w, h = self.size
        for marker in (1000, 500, 200, 100, 50, 20, 10, 5, 2, 1, 0.5, 0.2, 0.1):
            pixel_lenght = self.scale * marker
            if pixel_lenght < w / 4:
                pdraw = ImageDraw.Draw(self.canvas)
                pdraw.line([(w - 40 - pixel_lenght, 40), (w - 40, 40)], "black")
                pdraw.line(
                    [(w - 40 - pixel_lenght, 30), (w - 40 - pixel_lenght, 50)], "black"
                )
                pdraw.line([(w - 40, 30), (w - 40, 50)], "black")
                pdraw.text(
                    (w - 40 - pixel_lenght, 50),
                    f"{marker} m",
                    font=self.scale_font,
                    fill=(0, 0, 0, 255),
                )
                break
        else:
            assert False, "No valid scale"


if __name__ == "__main__":
    from math import sqrt
    from tqdm import tqdm
    Path("output").mkdir(exist_ok=True)
    with Animation("output/test", 10, bordersize=300) as a:
        #a.add_title("animation_test")
        #a.create_scale()
        for i in tqdm(range(20*25)):
            a.register_object(
                "human" if i < 16*25 else "human_dead", (0, 0), False, ""
            )
            distance = 15 - min(i,16*25)/50
            a.register_object("raptor", (0, -distance + 3), False, "")
            a.register_object("raptor", (distance * sqrt(3)/2, distance/2), False, "")
            a.register_object("raptor", (-distance * sqrt(3)/2, distance/2), True, "")
            #a.add_timestamp(i / 25)
            #a.add_label(f"index: {i}")
            a.flip_frame()
            
